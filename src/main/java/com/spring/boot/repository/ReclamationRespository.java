package com.spring.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.boot.model.Reclamations;;

@Repository("reclamationRepository")
public interface ReclamationRespository extends JpaRepository<Reclamations, Long> {
 
 Reclamations findBynom(String nom);
}
