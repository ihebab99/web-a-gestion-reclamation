package com.spring.boot.service;

import com.spring.boot.model.User;

public interface UserService {
  
 public User findUserByEmail(String email);
 
 public void saveUser(User user);
 public void saveReclamations(String civilite,String nom,String prenom,String Email,String tele,String adresse,String codepostal,String ville,String reclamations,String comments);
 public void consulteReclamation(String civilite,String nom,String prenom,String Email,String tele,String adresse,String codepostal,String ville,String reclamations,String comments);
}
