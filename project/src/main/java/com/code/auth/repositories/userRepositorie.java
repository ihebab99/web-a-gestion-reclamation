package com.code.auth.repositories;

import com.code.auth.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface userRepositorie extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
