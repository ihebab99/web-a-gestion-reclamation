package com.code.auth.repositories;

import com.code.auth.Model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepositorie extends JpaRepository<Role, Long>{
}