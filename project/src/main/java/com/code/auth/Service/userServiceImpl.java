package com.code.auth.Service;

import com.code.auth.Model.User;
import com.code.auth.repositories.RoleRepositorie;
import com.code.auth.repositories.userRepositorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class userServiceImpl implements userservice {
    @Autowired
    private userRepositorie userRepository;
    @Autowired
    private RoleRepositorie roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}