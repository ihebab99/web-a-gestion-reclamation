package com.code.auth.Service;

public interface SecuriteService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
