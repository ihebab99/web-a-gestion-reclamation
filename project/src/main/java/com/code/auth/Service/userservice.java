package com.code.auth.Service;

import com.code.auth.Model.User;

public interface userservice {
    void save(User user);

    User findByUsername(String username);
}
